package com.example.demo.controller;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.mailDao.User;
import com.example.demo.model.CandidateDetails;
import com.example.demo.model.EducationalDetails;
import com.example.demo.model.ExperienceDetails;

import com.example.demo.model.Login;
import com.example.demo.service.MailService;
import com.example.demo.service.PassService;

@RestController
public class WelcomeController {
	@Autowired
	public PassService serv;
	@Autowired
	private MailService notificationService;

	@Autowired
	private User user;


	@GetMapping("/")
	public ModelAndView homePage() {
		return new ModelAndView("NewFile", "pass", new Login());

	}

	@PostMapping("/add")
	public ModelAndView addLogin(@ModelAttribute Login pass, @RequestParam String password, HttpSession session) {
		Login userLogin = serv.getUserDetails(pass.getUsername());
		ModelAndView mav = new ModelAndView();
		String result = "";
		if (userLogin != null) {
			CandidateDetails candidateDetails = serv.getCandidateDetails(userLogin.getUserId());
			mav.addObject("candidateDetails", candidateDetails);
			mav.setViewName("NewFile1");
		} else if (userLogin == null && password.equals("123456")) {
			pass.setUserId(serv.generatingRandomAlphanumeric());
			// pass.setTime(serv.dateTime());
			serv.savePass(pass);
			session.setAttribute("loginDetails", pass);
			mav.addObject("loginDetails", pass);
			mav.setViewName("NewFile1");
			// ..result = "NewFile1";
			// return "NewFile1";
		} else {
			mav = new ModelAndView("redirect:/");
			// result = "redirect:/";
			// return "redirect:/";
		}
		return mav;
	}

	@PostMapping("/addDetails")
	@Transactional
	public String addPersonalDetails(HttpSession session, @ModelAttribute EducationalDetails edu,
			@ModelAttribute CandidateDetails cand, @ModelAttribute ExperienceDetails exp, @ModelAttribute Login pass,
			@RequestParam String userId) {

		System.out.println("userId:" + userId);
		CandidateDetails candDetails = serv.getCandidateDetails(userId);
		Login login = (Login) session.getAttribute("loginDetails");
		cand.setLoginDetails(login);
		serv.saveCand(cand);
		session.removeAttribute("loginDetails");
		CandidateDetails obj = serv.saveCand(cand);
		edu.setCandidate(obj);
		serv.saveEducation(edu);
		CandidateDetails obj1 = serv.saveCand(cand);
		exp.setCandidate(obj1);
		serv.saveExper(exp);

		return "NewFile2";
	}
	@RequestMapping("/send-mail-attachment")
	public String sendWithAttachment() throws MessagingException {

		/*
		 * Creating a User with the help of User class that we have declared and setting
		 * Email address of the sender.
		 */
		user.setEmailAddress("sahoojogesh96@gmail.com"); //Receiver's email address

		/*
		 * Here we will call sendEmailWithAttachment() for Sending mail to the sender
		 * that contains a attachment.
		 */
		try {
			notificationService.sendEmailWithAttachment(user);
		} catch (MailException mailException) {
			System.out.println(mailException);
		}
		return "Congratulations! Your mail has been send to the user.";
	}
}