package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="EducationalDetails")
public class EducationalDetails {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private long eduId;
private String name;
private String instituteName;
private Integer yearOfPassing;
private String specialisation;
private String boardOfEducation;
@ManyToOne
private CandidateDetails candidate;
public long getEduId() {
	return eduId;
}
public void setEduId(long eduId) {
	this.eduId = eduId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getInstituteName() {
	return instituteName;
}
public void setInstituteName(String instituteName) {
	this.instituteName = instituteName;
}
public Integer getYearOfPassing() {
	return yearOfPassing;
}
public void setYearOfPassing(Integer yearOfPassing) {
	this.yearOfPassing = yearOfPassing;
}
public String getSpecialisation() {
	return specialisation;
}
public void setSpecialisation(String specialisation) {
	this.specialisation = specialisation;
}
public String getBoardOfEducation() {
	return boardOfEducation;
}
public void setBoardOfEducation(String boardOfEducation) {
	this.boardOfEducation = boardOfEducation;
}
public CandidateDetails getCandidate() {
	return candidate;
}
public void setCandidate(CandidateDetails candidate) {
	this.candidate = candidate;
}
public EducationalDetails(long eduId, String name, String instituteName, Integer yearOfPassing, String specialisation,
		String boardOfEducation, CandidateDetails candidate) {
	super();
	this.eduId = eduId;
	this.name = name;
	this.instituteName = instituteName;
	this.yearOfPassing = yearOfPassing;
	this.specialisation = specialisation;
	this.boardOfEducation = boardOfEducation;
	this.candidate = candidate;
}
public EducationalDetails() {
	super();
	// TODO Auto-generated constructor stub
}
}