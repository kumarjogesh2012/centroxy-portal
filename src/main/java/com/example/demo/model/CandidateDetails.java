package com.example.demo.model;

import java.time.LocalDate;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CandidateDetails")
public class CandidateDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
 
	private int candId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String gender;
	private long mobileNo;
	private String emailId;
	private String status;
	private String hrApproval;
	private LocalDate CreatedDate;
	private String createdBy;
	private LocalDate modifiedDate;
	private String modifiedBy;
	@OneToMany
	private List<EducationalDetails> qualifications;
	@OneToMany
	private List<ExperienceDetails> details;
	@OneToOne
	@JoinColumn(name = "user_id")
	private Login loginDetails;
	public int getCandId() {
		return candId;
	}
	public void setCandId(int candId) {
		this.candId = candId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHrApproval() {
		return hrApproval;
	}
	public void setHrApproval(String hrApproval) {
		this.hrApproval = hrApproval;
	}
	public LocalDate getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(LocalDate createdDate) {
		CreatedDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDate getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(LocalDate modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public List<EducationalDetails> getQualifications() {
		return qualifications;
	}
	public void setQualifications(List<EducationalDetails> qualifications) {
		this.qualifications = qualifications;
	}
	public List<ExperienceDetails> getDetails() {
		return details;
	}
	public void setDetails(List<ExperienceDetails> details) {
		this.details = details;
	}
	public Login getLoginDetails() {
		return loginDetails;
	}
	public void setLoginDetails(Login loginDetails) {
		this.loginDetails = loginDetails;
	}
	
	public CandidateDetails(int candId, String firstName, String middleName, String lastName, String gender,
			long mobileNo, String emailId, String status, String hrApproval, LocalDate createdDate, String createdBy,
			LocalDate modifiedDate, String modifiedBy, List<EducationalDetails> qualifications,
			List<ExperienceDetails> details, Login loginDetails) {
		super();
		this.candId = candId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.mobileNo = mobileNo;
		this.emailId = emailId;
		this.status = status;
		this.hrApproval = hrApproval;
		CreatedDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
		this.qualifications = qualifications;
		this.details = details;
		this.loginDetails = loginDetails;
	}
	public CandidateDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
}