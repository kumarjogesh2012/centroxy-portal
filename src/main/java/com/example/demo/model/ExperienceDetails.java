package com.example.demo.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ExperienceDetails")
public class ExperienceDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String employeeName;
	private String designation;
	private String dateOfJoining;
	private String lastDate;
	private Integer noticePeriod;
	private double yearsOfExperience;
	
	@ManyToOne
	private CandidateDetails candidate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	public String getLastDate() {
		return lastDate;
	}
	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}
	public Integer getNoticePeriod() {
		return noticePeriod;
	}
	public void setNoticePeriod(Integer noticePeriod) {
		this.noticePeriod = noticePeriod;
	}
	public double getYearsOfExperience() {
		return yearsOfExperience;
	}
	public void setYearsOfExperience(double yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}
	public CandidateDetails getCandidate() {
		return candidate;
	}
	public void setCandidate(CandidateDetails candidate) {
		this.candidate = candidate;
	}
	public ExperienceDetails(Integer id, String employeeName, String designation, String dateOfJoining, String lastDate,
			Integer noticePeriod, double yearsOfExperience, CandidateDetails candidate) {
		super();
		this.id = id;
		this.employeeName = employeeName;
		this.designation = designation;
		this.dateOfJoining = dateOfJoining;
		this.lastDate = lastDate;
		this.noticePeriod = noticePeriod;
		this.yearsOfExperience = yearsOfExperience;
		this.candidate = candidate;
	}
	public ExperienceDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
}