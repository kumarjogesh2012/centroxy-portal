package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="process")
public class process {
	@Id
	private int process_id;
	private String process_name;
	public int getProcess_id() {
		return process_id;
	}
	public void setProcess_id(int process_id) {
		this.process_id = process_id;
	}
	public String getProcess_name() {
		return process_name;
	}
	public void setProcess_name(String process_name) {
		this.process_name = process_name;
	}
	public process(int process_id, String process_name) {
		super();
		this.process_id = process_id;
		this.process_name = process_name;
	}
	public process() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
