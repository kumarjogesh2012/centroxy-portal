package com.example.demo.model;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "Login")
public class Login {
	@Id
	private String userId;
	private String username;
	private String password;
	private String dateTime;		

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	/*
	 * public CandidateDetails getCandidate() { return candidate; }
	 * 
	 * public void setCandidate(CandidateDetails candidate) { this.candidate =
	 * candidate; }
	 */

	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Login(String userId, String username, String password, String dateTime) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.dateTime = dateTime;
		//this.candidate = candidate;
	}

	@PrePersist
	public void prePersist() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		this.dateTime = dtf.format(now);
	}
	
	@PreUpdate
	public void preUpdate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		this.dateTime = dtf.format(now);
	}
	
	}