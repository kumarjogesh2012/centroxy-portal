package com.example.demo.repository;

 

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

 

import com.example.demo.model.Login;

 

@Repository
public interface LoginRepository extends JpaRepository<Login, String> {

 

    @Query("FROM Login where username=:username")
    Login getUserDetails(@Param("username")String username);

 

}