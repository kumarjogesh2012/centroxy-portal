package com.example.demo.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.CandidateDetails;
import com.example.demo.model.EducationalDetails;

import com.example.demo.model.ExperienceDetails;

import com.example.demo.model.Login;
import com.example.demo.repository.CandidateRepository;

import com.example.demo.repository.EducationalRepository;
import com.example.demo.repository.ExperienceRepository;
import com.example.demo.repository.LoginRepository;

@Service
public class PassService {
	@Autowired
	public LoginRepository repo;
	@Autowired
	public CandidateRepository repo1;
	@Autowired
	public EducationalRepository repo2;
	@Autowired
	public ExperienceRepository repo3;

	public Login savePass(Login pass) {
		return repo.save(pass);
	}

	public CandidateDetails saveCand(CandidateDetails cand) {
		return repo1.save(cand);
	}

	public ExperienceDetails saveExper(ExperienceDetails exp) {
		return repo3.save(exp);
	}

	public EducationalDetails saveEducation(EducationalDetails edu) {
		return repo2.save(edu);
	}

	public String generatingRandomAlphanumeric() {
		String generatedString = RandomStringUtils.randomAlphanumeric(8);

		System.out.println(generatedString);
		return generatedString;
	}

	public String dateTime() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String sss = dtf.format(now);
		return sss;

	}

	public CandidateDetails getCandidateDetails(String userId) {
		CandidateDetails candidateDetails = repo1.getCandidateDetails(userId);
		return candidateDetails;
	}

	public Login getUserDetails(String username) {
		Login user = repo.getUserDetails(username);
		return user;
	}

}
