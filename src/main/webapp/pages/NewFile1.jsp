<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Screening form</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="footer, address, phone, icons" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script
src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


</head>
<style>
body {
font-family: Arial, Helvetica, sans-serif;
background-color: black;
}

* {
box-sizing: border-box;
}

.container {
padding: 10px;
background-color: white;
}

input[type=text], input[type=password] {
width: 30%;
padding: 15px;
margin: 5px 0 22px 0;
display: inline-block;
border: none;
background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
background-color: #ddd;
outline: none;
}

hr {
border: 1px solid #f1f1f1;
margin-bottom: 25px;
}

.registerbtn {
background-color: #4CAF50;
color: white;
padding: 16px 20px;
margin: 8px 0;
border: none;
cursor: pointer;
width: 20%;
opacity: 0.9;
}

.registerbtn:hover {
opacity: 1;
}

a {
color: dodgerblue;
}

.signin {
background-color: #f1f1f1;
text-align: center;
}

body {
background: white;
color: #013220;
}

div.well {
height: 250px;
}

.Absolute-Center {
margin: auto;
position: absolute;
top: 0;
left: 0;
bottom: 0;
right: 0;
}

.Absolute-Center.is-Responsive {
width: 50%;
height: 50%;
min-width: 200px;
max-width: 400px;
padding: 40px;
}

.container {
width: 100%;
margin: 0 auto;
}

.header {
overflow: hidden;
background-color: #f1f1f1;
padding: 20px 10px;
}

.header a {
float: left;
color: black;
text-align: center;
padding: 12px;
text-decoration: none;
font-size: 18px;
line-height: 25px;
border-radius: 4px;
}

.header a.logo {
font-size: 25px;
font-weight: bold;
}

.header a:hover {
background-color: #ddd;
color: black;
}

.header a.active {
background-color: dodgerblue;
color: white;
}

.header-right {
float: right;
}

@media screen and (max-width: 500px) {
.header a {
float: none;
display: block;
text-align: left;
}
.header-right {
float: none;
}
.footer {
height: 300;
width: 1200;
}
.container1 {
background-color: pink;
}

table.inner{
  border: 0px
}
}
</style>


<body>
<div class="header">
<a href="https://centroxy.com/"> <img border="0"
src=C:\Users\ROHAN\Desktop\CentroxyPortal></a>
<div class="header-right">

<a href="#" style="align-self: flex-start;"><span
class="glyphicon glyphicon-envelope"></span>hello@centroxy.com</a> <a
href="#" style="align-self: flex-start;"><span
class="glyphicon glyphicon-earphone"></span>+1-(908) 857-0100</a>

</div>
</div>


<center><form action="/addDetails?userId=${Login.userId}" method="post">
<input type="hidden" name="userId" value="${Login.userId}"/>
<div class="container">
<b><h3>Personal details</h3></b>
</div>
<hr>

<label for="fname"><b>First Name</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="firstName" >
<br>
<label for="fname"><b>Last Name</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="middleName" >
<br>

<label for="gender"><b>Gender</b></label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="radio" id="male" name="gender" value="male"> <label
for="male">Male</label> <input type="radio" id="female"
name="gender" value="female"> <label for="female">Female</label>
<input type="radio" id="other" name="gender" value="other">
<label for="other">Other</label> <br><br>

<label for="mobile"><b>Mobile
Number</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
type="number" name="mobileNo" > <br>

<label
for="email"><b>Email ID</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
type="text" name="emailId"> <br>

<!-- <label
for="position"><b>Curre</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="designation"><br>
 -->
<label for="expe"><b>Years
of Experience</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="text" name="yearsOfExperience" > <br><br>
<tr>


<td><b>Educational Details</b><br /><br /></td>
 
<td>
<table>

<tr>
<td align="center"><b>Sl.No.</b></td>
<td align ="center"><b>Institiute Name</b></td>
<td align="center"><b>Year of Passing</b></td>
<td align="center"><b>Specialization</b></td>
</tr>

 <tr>
<td>1.</td>
<td>Masters</td>
<td><input type="text" name="instituteName" maxlength="30" /></td>
<td><input type="text" name="yearOfPassing" maxlength="30" /></td>
<td><input type="text" name="specialisation" maxlength="30" /></td>
</tr>
 
<tr>
<td>2.</td>
<td>Graduation</td>
<td><input type="text" name="instituteName" maxlength="30" /></td>
<td><input type="text" name="yearOfPassing" maxlength="30" /></td>
<td><input type="text" name="specialisation" maxlength="30" /></td>
</tr>
<tr>
<td>3.</td>
<td>Class XII</td>
<td><input type="text" name="instituteName" maxlength="30" /></td>
<td><input type="text" name="yearOfPassing" maxlength="30" /></td>
<td><input type="text" name="specialisation" maxlength="30" /></td>
</tr>
<tr>
<td>4.</td>
<td>Class X</td>
<td><input type="text" name="instituteName" maxlength="30" /></td>
<td><input type="text" name="yearOfPassing" maxlength="30" /></td>
<td><input type="text" name="specialisation" maxlength="30" /></td>
</tr>
</tr>
</table>
</td>
</tr>
<br>
<br>
<p><b>Skills Known & Relevant experience : </b></p>

 <script type="text/javascript">
        function addField (argument) {
            var myTable = document.getElementById("myTable");
            var currentIndex = myTable.rows.length;
            var currentRow = myTable.insertRow(-1);
           
            var linksBox = document.createElement("input");
            linksBox.setAttribute("name", "skills" + currentIndex);

            var keywordsBox = document.createElement("input");
            keywordsBox.setAttribute("name", "Experience" + currentIndex);

            var addRowBox = document.createElement("input");
            addRowBox.setAttribute("type", "button");
            addRowBox.setAttribute("value", " + ");
            addRowBox.setAttribute("onclick", "addField();");
            addRowBox.setAttribute("class", "button");

            var currentCell = currentRow.insertCell(-1);
            currentCell.appendChild(linksBox);
currentCell = currentRow.insertCell(-1);
            currentCell.appendChild(keywordsBox);    
           
        }
    </script>
 
    <button onclick="myFunction()"> - </button>
<script>
function myFunction() {
  document.getElementById("myTable").deleteRow(1);
 
}
</script>
   

    <table id="myTable">
        <tr>
        <th>Skills</th>
        <br>
    <td><input type="button" class="button" value="+" onclick="addField();"></td>
    <th>Experience</th>
        </tr>
    </table>


<br> <label for="job"><b>Reason for job change</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="text" name="job" > <br> <br>
<b><p>How did you know about Centroxy:</p></b> <br> <br> <label
for="direct"><b>1. Direct (Through Centroxy)</b></label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="radio" id="y" name="choose" value="y"> <label
for="y">Yes</label> <input type="radio" id="n" name="choose"
value="n"> <label for="n">No</label> <br> <br> <label
for="ad"><b>2. Response to an advertisement </b></label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="radio" id="portal" name="choose1" value="portal">
<label for="portal">Job Portals</label> <input type="radio"
id="website" name="choose1" value="website"> <label
for="website">Website</label> <br> <br> <label for="ref"><b>3.Referred
by Our Employee </b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
type="text" name="ref" placeholder="Enter his/her name">
<br> <label for="Placement"><b>4.Placement
Consultant</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="text" name="Placement"
placeholder="Please specify if any"> <br> <label
for="Others"><b>5.Others</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="text" name="Others" placeholder="Please specify if any"> <br> <br> <br> <b><p>WorkDetails:</p></b>
<br> <!-- <label for="cc"><b>Current Company</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="cc"
> <br> --> <label for="des"><b>Current
Designation</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp; <input type="text" name="designation" > <br>

<label for="doj"><b>Date Of Joining</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 <input type="date" name="date_of_joining" > <br><br>
 
 <label
for="np"><b>Notice Period</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp; <input type="text" name="notice_period" > <br> <label
for="exper"><b>Experience </b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="years_of_experience"
placeholder="Experience with the Company in years" >
<br> <br> <label for="work"><b>Willing to work
on-site (Any location as decided by the company)</b></label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="radio" id="work" name="choose2" value="work"> <label
for="work">Yes</label> <input type="radio" id="onsite"
name="choose2" value="onsite"> <label for="onsite">No</label>
<br> <label for="work"><b>Willing to work in night
shifts </b></label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" id="night"
name="choose3" value="night"> <label for="night">Yes</label>
<input type="radio" id="shift" name="choose3" value="shift">
<label for="shift">No</label> <br> <br> <label
for="myfile">Updated CV:</label> <input type="file" id="myfile"
placeholder="Max size 500kb" name="myfile"><br>
<br> <input type="button" value="Upload">
<hr>
</center>
<b>Declaration:</b>
<br>
<br>
<input type="checkbox" id="first" name="vec" value="option1">
<label for="vec">I, hereby confirm that the information
provided by me in this application form are correct and accurate.</label>
<br>

<input type="checkbox" id="second" name="vec2" value="option2">
<label for="vec1">I understand that in the event of my
employment, if any information provided here by me is found to be
incorrect, my employment may be terminated without any notice.</label>
<br>
<br>
<label for="myfile">Upload your Signature:</label>
<input type="file" id="myfile" placeholder="Max size 500kb" name="myfile">
<input type="button" value="Upload">
<br>
<label for="date"><b>Date:</b></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="date" name="date"   > <br>
<a href="/send-mail-attachment">mail</a>

<br>
<br>
<br>
<br>
</div>
<center>
<button>Submit</button>
</center>

</div>
</form>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="header">
<div class="rows">
<div class="col-sm-4">
<a href="https://centroxy.com/"> <img border="0"
src="centroxy.png"></a> <br> <br>
<br>
<p>
<font color="black"> Centroxy is mainly focused on giving
perfect solutions and makes clients feel more convenient.We
provide you a complete range of services without compromising on
ethical business standards.
</p>
</font> <br>

</div>
<div class="col-sm-4">
<font color="black">
<h4>
<b>INDUSTRIES</b>
</h4> Banking & Financial Services <br> Insurance <br>
Technology <br> Travel & Hospitality <br> Logistics <br>
Healthcare

</font>
</div>
</div>

<div class="col-sm-4">
<font color="black">
<h4>
<b>CONTACT</b>
</h4> <b> Centroxy Solution Pvt. Ltd.</b> 3rd Floor, OCAC Tower, Acharya
Vihar, Bhubaneswar-751013, Odisha, INDIA <br> <b> Centroxy
Solution FZCO</b> Dubai Airport Free Zone 3W, G016 P.O. Box 371604,
Dubai,UAE <br> <b>Centroxy Computer Systems LLC</b> 108B, Abdul
Razaq Ali Hassan Alzarouni Hor Al Anz East, Dubai,UAE <br> <br>
<b>Phone:</b> <br> <b>US:</b> +1-(908) 857-0100 <br> <b>India:</b>
+91 9339898121 <br> <b>Dubai:</b>+971-501735957 <br> <b>CIN:</b>
U72200OR2016PTC019795</b>


</font>
<br>


</div>
</body>
</html>